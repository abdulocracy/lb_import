# lb_import

[ListenBrainz](https://listenbrainz.org) allows you to export your scrobbles as a .json file. This tool allows you to import them back into an account.

## Usage

lb_import depends on [pylistenbrainz](https://github.com/paramsingh/pylistenbrainz).

Once the dependency is installed you can run an import with the following command:

`lb_import.py MY-API-KEY scrobbles.json`

Importing the same dump multiple times will not create duplicates.
